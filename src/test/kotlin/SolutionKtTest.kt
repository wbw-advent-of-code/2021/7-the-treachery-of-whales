import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

internal class SolutionKtTest {
    @Test
    internal fun `calculate fuel part 1 returns 37`() {
        assertThat(part1("src/test/resources/input.txt"))
            .isEqualTo(37)
    }
}